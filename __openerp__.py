#  -*- coding: utf-8 -*-

{
    "name": "Organisation de travail",
    "version": "0.1",
    "author": "mos",
    "category": 'laboite',
    'complexity': "easy",
    "description": """Organisation de travail""",
    'website': '',
    'images': [],
    'init_xml': [],
    "depends": ["project"],
    'data': [
            "views/task.xml",
             
           ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
