# -*- coding: utf-8 -*-
from openerp.tools.translate import _
from openerp.osv import fields, osv
from datetime import datetime

class task(osv.osv):
    _name = "project.task"
    _inherit = "project.task"
    _columns = {
        'task_fils_ids': fields.one2many('project.task', 'task_parent_id', 'Task files'),
        'task_parent_id': fields.many2one('project.task', 'Task parent'),
    }

# {'date_deadline': '2014-12-18'}
# {'task_fils_ids': [[1, 1, {'date_deadline': '2014-12-29'}], [4, 3, False]]}
# {'task_fils_ids': [[4, 1, False], [1, 3, {'date_deadline': '2014-12-30'}]]}
# {'task_fils_ids': [[1, 1, {'description': 'aaaaaaaaaaaaa'}], [4, 3, False]], 'date_deadline': '2014-12-30'}
# {'date_start': '2014-12-25 10:20:20'}
# {'task_parent_id': 23}
    def write(self, cr, uid, ids, vals, context=None):
        print "before"
        print vals
                
        if vals.get('date_end') and not vals.get('task_parent_id') :
            
            task_fils_ids = []
            for task1 in self.browse(cr, uid, ids, context=None):
                if task1.date_end:
                    old_date = datetime.strptime(task1.date_end, '%Y-%m-%d %H:%M:%S')
                    new_date = datetime.strptime(vals.get('date_end'), '%Y-%m-%d %H:%M:%S')
                    difirence = new_date - old_date
                    task_fils_ids = []
                    for tax_fils in task1.task_fils_ids:
                        date_start = datetime.strptime(tax_fils.date_start, '%Y-%m-%d %H:%M:%S') + difirence  
                        date_end = datetime.strptime(tax_fils.date_end, '%Y-%m-%d %H:%M:%S') + difirence  
                        date_start = '' + str(date_start.year) + '-' + str(date_start.month) + '-' + str(date_start.day) + ' ' + str(date_start.hour) + ':' + str(date_start.minute) + ':'+ str(date_start.second)
                        date_end = '' + str(date_end.year) + '-' + str(date_end.month) + '-' + str(date_end.day) + ' ' + str(date_end.hour) + ':' + str(date_end.minute) + ':'+ str(date_end.second)
                        tax_fils_vals = [1, tax_fils.id, {'date_start': date_start ,'date_end': date_end  } ]
                        task_fils_ids.append(tax_fils_vals)
            if not vals.get('task_fils_ids'):
                vals.update({
                        'task_fils_ids': task_fils_ids
                        })
                
            else :
                for line in vals.get('task_fils_ids'):
                    if line[1]:
                        if not line[2]:
                            line[2] = {}
                            line[0] = 1
                        for l in task_fils_ids:
                            if l[1] == line[1]:
                                line[2].update({'date_end':l[2]['date_end'] ,'date_start': l[2]['date_start']  })
        
        
        elif vals.get('task_parent_id'):
            for task1 in self.browse(cr, uid, vals.get('task_parent_id'), context=None):
                
                for tax_fils in self.browse(cr, uid, ids, context=None):
                    duree_tax_fils = datetime.strptime(tax_fils.date_end, '%Y-%m-%d %H:%M:%S') - datetime.strptime(tax_fils.date_start, '%Y-%m-%d %H:%M:%S')
                    date_start =  datetime.strptime(task1.date_end, '%Y-%m-%d %H:%M:%S')  
                    date_end = date_start + duree_tax_fils 
                    date_start = '' + str(date_start.year) + '-' + str(date_start.month) + '-' + str(date_start.day) + ' ' + str(date_start.hour) + ':' + str(date_start.minute) + ':'+ str(date_start.second)
                    date_end = '' + str(date_end.year) + '-' + str(date_end.month) + '-' + str(date_end.day) + ' ' + str(date_end.hour) + ':' + str(date_end.minute) + ':'+ str(date_end.second)
                    vals.update({'date_start': date_start ,'date_end': date_end })
            

        print "after"
        print vals
         
        res = super(task, self).write(cr, uid, ids, vals, context=context)
        
        return res



# before date_end
# {'task_fils_ids': [[1, 13, {'description': '11111111111111111111'}], [4, 14, False]], 'date_deadline': '2014-11-30'}
# after
# {'task_fils_ids': [[1, 13, {'date_start': '2014-11-30 0:0:0', 'description': '11111111111111111111', 'date_deadline': '2014-12-1'}], [4, 14, {'date_start': '2014-11-30 0:0:0', 'date_deadline': '2014-12-2'}]], 'date_deadline': '2014-11-30'}
# before
# {'date_start': '2014-11-30 0:0:0', 'description': '11111111111111111111', 'date_deadline': '2014-12-1'}
# after
# {'date_start': '2014-11-30 0:0:0', 'task_fils_ids': [], 'description': '11111111111111111111', 'date_deadline': '2014-12-1'}